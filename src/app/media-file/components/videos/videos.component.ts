import { Component, OnInit } from '@angular/core';
import { JSONHttpService } from "../../../services/jsonhttp.service";
import { Http } from '@angular/http';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit {

  shipMediaDetails ={
    images:[],
    videos:[]
  };
  constructor(private JSONHttp: JSONHttpService,private http:Http) {}

  ngOnInit() {
    this.getShipMediaDetails();
  }
  getShipMediaDetails() {
    this.JSONHttp.get("https://api.myjson.com/bins/1e3702", (data, err) => {
      if (err) {
      } else {
        this.shipMediaDetails = data.json();
        console.log(this.shipMediaDetails);
      }
    });
  }

}
