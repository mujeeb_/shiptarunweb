import { Component, OnInit } from '@angular/core';
import { JSONHttpService } from "../../../services/jsonhttp.service";
import { Http } from '@angular/http';
@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit {

  constructor(private JSONHttp: JSONHttpService,private http:Http) {}
  shipMediaDetails ={
    images:[]
  };
  ngOnInit() {
    this.getShipMediaDetails();
  }
  getShipMediaDetails() {
    this.JSONHttp.get("https://api.myjson.com/bins/1e3702", (data, err) => {
      if (err) {
      } else {
        this.shipMediaDetails = data.json();
        console.log(this.shipMediaDetails);
      }
    });
  }

}
