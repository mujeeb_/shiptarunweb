import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GalleryComponent } from './components/gallery/gallery.component';


import { RouterModule, Routes } from '@angular/router';
import { VideosComponent } from './components/videos/videos.component';
import { ImagesComponent } from './components/images/images.component';
import { PdfComponent } from './components/pdf/pdf.component';

const routers:Routes = [
  {
    path:'',
    component : GalleryComponent,
    children: [
      {
        path: "",
        component: VideosComponent 
      },
      {
        path: "videos",
        component: VideosComponent 
      },{
        path: "images",
        component: ImagesComponent 
      },  {
        path: "pdf",
        component: PdfComponent 
      }   
    ]
  }
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routers)
  ],
  declarations: [GalleryComponent, VideosComponent, ImagesComponent, PdfComponent],
  
})
export class MediaFileModule { }
