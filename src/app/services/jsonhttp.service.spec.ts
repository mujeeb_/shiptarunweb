import { TestBed, inject } from '@angular/core/testing';

import { JsonhttpService } from './jsonhttp.service';

describe('JsonhttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JsonhttpService]
    });
  });

  it('should be created', inject([JsonhttpService], (service: JsonhttpService) => {
    expect(service).toBeTruthy();
  }));
});
