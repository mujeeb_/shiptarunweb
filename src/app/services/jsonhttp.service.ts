import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class JSONHttpService {

  constructor(private http: Http) { }

  options = new RequestOptions({
      headers: new Headers({
          "Content-Type": "application/json"
      })
  })

  get(url, cb) {
      this.http.get(url).subscribe(data => {
          cb(data)
      }, err => {
          cb(null, err)
      });
  }
}