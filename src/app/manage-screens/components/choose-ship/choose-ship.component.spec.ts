import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseShipComponent } from './choose-ship.component';

describe('ChooseShipComponent', () => {
  let component: ChooseShipComponent;
  let fixture: ComponentFixture<ChooseShipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseShipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseShipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
