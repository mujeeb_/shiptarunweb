import { Component, OnInit } from "@angular/core";
declare var $: any;
import { JSONHttpService } from "../../../services/jsonhttp.service";
import { Http, Response, Headers } from "@angular/http";

@Component({
  selector: "app-choose-ship",
  templateUrl: "./choose-ship.component.html",
  styleUrls: ["./choose-ship.component.css"]
})
export class ChooseShipComponent implements OnInit {
  
  constructor(private JSONHttp: JSONHttpService, private http: Http) {
    
  }

  num = 1;
  urlOfData = "";
  mediaScreen;
  newScreen;
  output: "";
  ships;
  addselectedShip = { shipName: "" };
  index = "";

  ngOnInit() {
    this.getShipName();
  }
  getShipName() {
    this.JSONHttp.get(`https://api.myjson.com/bins/b5g1e`, (data, err) => {
      if (err) {
      } else {
        this.ships = data.json();
        console.log(this.ships);
      }
    });
  }
  getShipMediaScreen(i) {
    this.mediaScreen = null;
    switch (i) {
      case 0:
        this.urlOfData = `https://api.myjson.com/bins/13ujea`;
        break;
      case 1:
        this.urlOfData = `https://api.myjson.com/bins/1d42f2`;
        break;
      case 2:
        this.urlOfData = `https://api.myjson.com/bins/ls9r2`;
        break;
      case 3:
        this.urlOfData = `https://api.myjson.com/bins/sxh0e`;
        break;
      case 4:
        this.urlOfData = `https://api.myjson.com/bins/11x48u`;
        break;
    }
    this.JSONHttp.get(this.urlOfData, (data, err) => {
      if (err) {
      } else {
        this.mediaScreen = data.json();
        console.log(this.mediaScreen);
      }
    });
  }

  submit() {}
  /**modal add Media for ship name */
  addMedia(shipName) {
    this.addselectedShip = shipName;
  }

  dropToggle(i) {
    this.index = i;
    console.log(i);
    this.num++;
    if(this.num %2 == 0){   
    this.getShipMediaScreen(i);
    
    }
    
  }
}
