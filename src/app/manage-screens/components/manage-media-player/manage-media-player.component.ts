import { Component, OnInit } from '@angular/core';
import { NONE_TYPE } from '@angular/compiler/src/output/output_ast';
import { JSONHttpService } from "../../../services/jsonhttp.service";

import { Http } from '@angular/http';

@Component({
  selector: 'app-manage-media-player',
  templateUrl: './manage-media-player.component.html',
  styleUrls: ['./manage-media-player.component.css']
})
export class ManageMediaPlayerComponent implements OnInit {
  numbers = [0];

  
  shipMediaDetails ={
    images:[],
    videos:[]
  };
  

	num = 1;
  constructor(private JSONHttp: JSONHttpService,private http:Http) {}

  ngOnInit() {
    this.getShipMediaDetails();
  }
  add() {
		this.num++;
		this.numbers = Array(this.num).fill(1);
  }
  minus(){
    this.num--;
    this.numbers =Array(this.num).fill(1);
  }
  getShipMediaDetails() {
    this.JSONHttp.get("https://api.myjson.com/bins/1e3702", (data, err) => {
      if (err) {
      } else {
        this.shipMediaDetails = data.json();
        console.log(this.shipMediaDetails);
      }
    });
  }
  

}
  
