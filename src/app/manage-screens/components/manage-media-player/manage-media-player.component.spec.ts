import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageMediaPlayerComponent } from './manage-media-player.component';

describe('ManageMediaPlayerComponent', () => {
  let component: ManageMediaPlayerComponent;
  let fixture: ComponentFixture<ManageMediaPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageMediaPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageMediaPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
