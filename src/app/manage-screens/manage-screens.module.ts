import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ScreensComponent } from "./components/screens/screens.component";


import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { ChooseShipComponent } from "./components/choose-ship/choose-ship.component";
import { ManageMediaPlayerComponent } from "./components/manage-media-player/manage-media-player.component";

const routers: Routes = [
  {
    path: "",
    component: ScreensComponent,
    children: [
      {
        path: "manageScreen/:ship_id/:screen_id",
        component: ManageMediaPlayerComponent
      }
    ]
  }
];
@NgModule({
  imports: [CommonModule,
     FormsModule, RouterModule.forChild(routers)],
  declarations: [
    ScreensComponent,
    ChooseShipComponent,
    ManageMediaPlayerComponent
  ]
})
export class ManageScreensModule {}
